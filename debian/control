Source: debvm
Maintainer: Helmut Grohne <helmut@subdivi.de>
Build-Depends: debhelper-compat (= 13), perl
Section: admin
Priority: optional
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://salsa.debian.org/helmutg/debvm/
Vcs-Git: https://salsa.debian.org/helmutg/debvm.git -b debian
Vcs-Browser: https://salsa.debian.org/helmutg/debvm/

Package: debvm
Architecture: all
Depends:
 ${misc:Depends},
 dpkg-dev,
 e2fsprogs,
 mmdebstrap (>= 1.3.0),
 qemu-system-native | qemu-kvm (>> 1:8.0) | qemu-system-any,
# Required for -device virtio-net-pci,netdev=net0
 ipxe-qemu,
Recommends:
 arch-test,
 file,
 systemd | binfmt-support,
 openssh-client,
 qemu-system,
 qemu-user-binfmt (>= 1:9.1.0+ds-1~) | qemu-user-static (<< 1:9.1.0+ds-1~),
 seabios,
 uidmap,
Suggests:
 qemu-system-gui,
Description: create and run virtual machines for various Debian releases and architectures
 The tool debvm-create can be used to create a virtual machine image and
 the tool debvm-run can be used to run such a machine image. Their purpose
 primarily is testing software using qemu as a containment technology.
 These are relatively thin wrappers around mmdebstrap and qemu.
