#!/bin/sh
# Copyright 2023 Johannes Schauer Marin Rodrigues <josch@debian.org>
# SPDX-License-Identifier: MIT
#
# Add a non-root user, add them to the sudo group and use the same authorized
# ssh keys as the root user.
#
#  - the new user is called "user" by default (customizable via $USERADDHOOK_USERNAME)
#  - no password required for login
#  - requires the passwd and coreutils packages installed inside the chroot
#  - adds the new user to the sudo group if it exists
#  - ~/.ssh/authorized_keys files is copied from root user if it exists
#  - enables immediate autologin via lightdm if installed
#
# Example usage:
#
#     $ debvm-create -k ~/.ssh/id_rsa.pub -- --hook-dir=.../useraddhook --include sudo
#     $ debvm-run -s 8022
#     $ ssh -l user -p 8022 127.0.0.1 whoami
#     user
#     $ ssh -l user -p 8022 127.0.0.1 sudo whoami
#     root
#

set -eu

: "${USERADDHOOK_USERNAME:=user}"

chroot "$1" useradd --home-dir "/home/$USERADDHOOK_USERNAME" --create-home --shell /bin/bash "$USERADDHOOK_USERNAME"
chroot "$1" passwd --delete "$USERADDHOOK_USERNAME"
if chroot "$1" getent group sudo >/dev/null; then
	echo "Adding $USERADDHOOK_USERNAME to sudo group"
	chroot "$1" usermod --append --groups sudo "$USERADDHOOK_USERNAME"
fi
if [ -e "$1"/root/.ssh/authorized_keys ]; then
	echo "Installing ssh authorized_keys for $USERADDHOOK_USERNAME"
	chroot "$1" install -o "$USERADDHOOK_USERNAME" -g "$USERADDHOOK_USERNAME" -m 700 -d "/home/$USERADDHOOK_USERNAME/.ssh"
	chroot "$1" install -o "$USERADDHOOK_USERNAME" -g "$USERADDHOOK_USERNAME" -t "/home/$USERADDHOOK_USERNAME/.ssh" /root/.ssh/authorized_keys
fi
if [ -e "$1/etc/lightdm/lightdm.conf" ]; then
	echo "Enabling autologin in lightdm for $USERADDHOOK_USERNAME"
	cat >>"$1/etc/lightdm/lightdm.conf" <<EOF
[SeatDefaults]
autologin-user=$USERADDHOOK_USERNAME
autologin-user-timeout=0
EOF
fi
if [ -e "$1/etc/greetd/config.toml" ] && [ -e "$1/usr/bin/sway" ]; then
	echo "Enabling autologin in greetd/sway for $USERADDHOOK_USERNAME"
	cat >>"$1/etc/greetd/config.toml" <<EOF
[initial_session]
command = "sway"
user = "$USERADDHOOK_USERNAME"
EOF
fi
