#!/bin/sh
#
# Copyright 2022 Helmut Grohne <helmut@subdivi.de>
# SPDX-License-Identifier: MIT
#
# This is a mmdebstrap customize hook that installs a kernel image. The name
# of the kernel image depends on the architecture, derivative and release.

set -eu

TARGET="$1"

if dpkg-query --root="$TARGET" --showformat='${db:Status-Status}\n' --show 'linux-image-*' 2>/dev/null | grep -q '^installed$'; then
	exit 0
fi

ARCHITECTURES=$(xargs < "$TARGET/var/lib/dpkg/arch")

KERNEL_ARCH="${ARCHITECTURES%% *}"
case "$KERNEL_ARCH" in
	armel)
		KERNEL_ARCH=rpi
		case "$ARCHITECTURES " in
			*" arm64 "*) KERNEL_ARCH=arm64:arm64 ;;
			*" armhf "*) KERNEL_ARCH=armmp:armhf ;;
		esac
	;;
	armhf)
		KERNEL_ARCH=armmp
		case "$ARCHITECTURES " in
			*" arm64 "*) KERNEL_ARCH=arm64:arm64 ;;
		esac
	;;
	hppa)
		KERNEL_ARCH=parisc
	;;
	i386)
		KERNEL_ARCH=686-pae
		case "$ARCHITECTURES " in
			*" amd64 "*) KERNEL_ARCH=amd64:amd64 ;;
		esac
	;;
	mips64el)
		KERNEL_ARCH=5kc-malta
	;;
	mipsel)
		KERNEL_ARCH=4kc-malta
		case "$ARCHITECTURES " in
			*" mips64el "*) KERNEL_ARCH=5kc-malta:mips64el
		esac
	;;
	ppc64)
		KERNEL_ARCH=powerpc64
	;;
	ppc64el)
		KERNEL_ARCH=powerpc64le
	;;
esac

export APT_CONFIG="$MMDEBSTRAP_APT_CONFIG"

if test "${MMDEBSTRAP_MODE:-}" = chrootless; then
	set -- \
		-oDPkg::Options::=--force-not-root \
		-oDPkg::Options::=--force-script-chrootless \
		-oDPkg::Options::=--root="$TARGET" \
		-oDPkg::Options::=--log="$TARGET/var/log/dpkg.log"
else
	set -- -oDPkg::Chroot-Directory="$TARGET"
fi

# On some derivatives such as Ubuntu, linux image does not depend on an initramfs.
apt-get --yes satisfy "$@" "linux-image-cloud-$KERNEL_ARCH | linux-image-$KERNEL_ARCH | linux-image-generic" "initramfs-tools | linux-initramfs-tool"
