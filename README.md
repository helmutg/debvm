What is debvm?
==============

The purpose of this tool is creating and running simple virtual machines based
on Debian releases. A typical application is testing. Consequently, this has
been designed to work without root privileges[^1]. Rather than do thing itself,
it builds primarily on
[mmdebstrap](https://gitlab.mister-muffin.de/josch/mmdebstrap/) and
[qemu](http://www.qemu.org/).

How do you use it?
==================

The first tool is `debvm-create`. It creates a rootfs ext4 raw filesystem
image. This image has configurable architecture, hostname, root's ssh key,
Debian mirror, and release as well as image size and included packages. In
essence, it is a wrapper constructing a complex `mmdebstrap` invocation.

Given such an image, `debvm-run` can be used to run it. It extracts the kernel
and initrd from the filesystem image and constructs a suitable `qemu`
invocation.

The following two invocations will give you a shell inside a qemu virtual
machine of the native architecture, leaving all the settings at their defaults:

    ./bin/debvm-create && ./bin/debvm-run

What do I need?
===============

A regular user account[^1] suffices. The following Debian packages should be
installed:
 * `arch-test` (when running foreign images)
 * `dpkg-dev`
 * `e2fsprogs`
 * `mmdebstrap`
 * `qemu-system-something`
 * `qemu-user-static` (when creating foreign images)
 * `uidmap` (for using user namespaces)

What is this image format precisely?
====================================

The image is a sparse ext4 file system image with a non-empty disk label. It
contains the root filesystem of a (Debian) installation including an init
system and a kernel. There is no partition table or bootloader. The following
paths are assumed inside:
 * `/bin/true` is used to detect the architecture of an image
 * `(|/boot)/vmlinu[xz]` must be a symbolic link pointing to a regular file
   containing the kernel.
 * `initrd.img` must be a symbolic link in the same directory as the kernel
   image pointing to a regular file containing the initrd image.

Why?
====

Most other tools in this area either do whole system emulation and include a
bootloader and partition table or share the Linux kernel with a container
image. This approach sits in between and allows emulating the Linux kernel
without getting into the complexity of dealing with different bootloaders by
using the one included in qemu.

The other aspect is restricting to Debian-based systems. This allows for a lot
of simplification of the problem space.

While most similar tools require root privileges at some point, this one works
with a subuid allocation for user namespaces, which is often available.

The implementation is so short that it still is feasible to read and understand
it. Let's see how long that lasts.

License
=======

The debvm tools are licensed under the MIT license.

Contributors
============

 * Arnd Bergmann
 * Gioele Barabucci
 * Helmut Grohne (main author)
 * Jochen Sprickerhof
 * Johannes Schauer Marin Rodrigues (main author of `mmdebstrap`)

[^1] This technically is a lie. It employs user namespaces and thus requires
     the setuid binary `newuidmap` as well as a suitable subuid allocation.
